#!/usr/bin/env bash
# delete every other file in a directory.
# this can be useful for gleaming old backups,
# amoung other tasks with customization
c=0
for i in $@
	do m=$(expr $c % 2)
	if [ "$m" -eq "1" ]
	then
		echo "Deleting $i"
		#rm -f "$i"
	else
		echo "leaving $i"
	fi
	let "c = c + 1"
done
