#!/usr/bin/env perl -w

use strict;

use File::Copy;

my $count = 0;
my $newfile = "";
while (<>) {
	chomp;
	$newfile = $_;
	$newfile =~ s/ /_/g;
	print $newfile . "\n";
	move($_, $newfile);
}
